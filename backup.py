#!/usr/bin/python
import ConfigParser
import os
import argparse
__author__ = 'dzubchik'
 

parser = argparse.ArgumentParser(description='This is wrapper to obnam backup util.')
parser.add_argument('--servers', type=str, default='*',nargs='*', help='servers to backup',required=False)
parser.add_argument('--folders', type=bool, default=False,choices=[True,False], help='disable /etc and /root folders',required=False)
parser.add_argument('--debug', type=bool, default=False,choices=[True,False], help='enable debug mode',required=False)
args = parser.parse_args()

## show values ##

"""returns default options"""
def options(name):
    return {
        'folders':['/etc','/root'],
        'keep'   :'30d',
        'user'   :'root',
        'config' :'config',
        'ns'     :'value not set'
    }.get(name,'ns')

def backup(host):
    folders = [] # list of all folders to backup
    user = config.get(host,'user') if config.has_option(host,'user') else options('user')
    hostname = 'sftp://'+user+'@'+config.get(host,'host')
    if config.has_option(host,'folders'):
        folders = config.get(host,'folders').split('\n')
        folders.pop(0)
    folders = folders+options('folders') if args.folders else folders
    folders = " ".join([hostname+':{0}'.format(element) for element in folders])
    client = config.get(host,'name') if config.has_option(host,'name') else host
    keep = config.get(host,'keep') if config.has_option(host,'keep') else options('keep')
    command = "obnam backup {folders} --client-name={client} --keep={keep} >/dev/null 2>&1 &".format(folders=folders, client=client,keep=keep)
    if(args.debug):
        print command
    else:
        os.system(command)

            

config = ConfigParser.RawConfigParser()
config.read(options('config'))
servers= config.sections() if (args.servers=='*') else args.servers

if args.debug:
    print ("Servers: %s" % args.servers )
    print ("Folders: %s" % args.folders )

for server in servers:
    backup(server)
